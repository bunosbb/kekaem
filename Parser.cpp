#include "Parser.h"

bool Get_char(std::istream& in, char& ch) {

    while (in >> ch) {
        if (ch != ' ') {
            return true;
        }
    }
    return false;
}

Object::Object() : type(Null) {}

Object::Object(int value) : type(Int), int_val(value) {}
Object::Object(std::string value) : type(String), str_val(value) {}
Object::Object(std::vector<Object> value) : type(Vector) {
    std::swap(vector_val, value);
}
Object::Object(std::unordered_map<std::string, Object> value) : type(Dict) {
    std::swap(dict_val, value);
}

bool Object::IsString() {
    return type == String;
}
bool Object::IsInt() {
    return type == Int;
}
bool Object::IsNull() {
    return type == Null;
}
bool Object::IsVector() {
    return type == Vector;
}
bool Object::IsDict() {
    return type == Dict;
}

std::string Object::AsString() const  {
    return str_val;
}
int Object::AsInt() const  {
    return int_val;
}
std::vector<Object>& Object::AsVector() {
    return vector_val;
}
std::unordered_map<std::string, Object>& Object::AsDict() {
    return dict_val;
}

Object& Object::operator =(std::string other) {
    type = String;
    str_val = other;
    return *this;
}


Object& Object::operator =(int other) {
    type = Int;
    int_val = other;
    return *this;
}

bool Object::ReadString(std::istream& in) {
    type = String;
    char in_ch;
    while (in >> in_ch) {
        if (in_ch == '"')
            return true;
        str_val += in_ch;
    }
    return false;
}
bool Object::ReadVector(std::istream& in) {
    type = Vector;
    char in_ch;
    Get_char(in, in_ch);
    while (in_ch != ']') {
        Object item;
        switch (in_ch) {
        case '"':
            item.ReadString(in);
            break;
        case '[':
            item.ReadVector(in);
            break;
        case '{':
            item.ReadDict(in);
            break;
        default:
            std::string int_str;
            while ('0' <= in_ch && in_ch <= '9') {
                int_str += in_ch;
                in_ch = in.peek();
                if ('0' > in_ch || in_ch > '9') {
                    break;
                }
                Get_char(in, in_ch);
            }
            item.type = Int;
            item.int_val = std::stoi(int_str);
            break;
        }
        vector_val.push_back(item);
        Get_char(in, in_ch);
        if (in_ch == ']')
            return true;
        if (in_ch != ',') {
            std::cerr << "cant read vector\n";
            return false;
        }
        Get_char(in, in_ch);
    }
    return true;
}

bool Object::ReadDict(std::istream& in) {
    type = Dict;
    char in_ch;
    Get_char(in, in_ch);
    while (in_ch != '}') {
        if (in_ch != '"') {
            std::cerr << "cant read dict\n";
            return false;
        }
        Object name;
        name.ReadString(in);
        Get_char(in, in_ch);
        if (in_ch != ':') {
            std::cerr << "cant read dict\n";
            return false;
        }
        Get_char(in, in_ch);
        Object item;
        switch (in_ch) {
        case '"':
            item.ReadString(in);
            break;
        case '[':
            item.ReadVector(in);
            break;
        case '{':
            item.ReadDict(in);
            break;
        default:
            std::string int_str;
            while ('0' <= in_ch && in_ch <= '9') {
                int_str += in_ch;
                in_ch = in.peek();
                if ('0' > in_ch || in_ch > '9') {
                    break;
                }
                Get_char(in, in_ch);
            }
            item.type = Int;
            item.int_val = std::stoi(int_str);
            break;
        }
        dict_val[name.AsString()] = item;
        Get_char(in, in_ch);
        if (in_ch == '}')
            return true;
        if (in_ch != ',') {
            std::cerr << "cant read dict\n";
            return false;
        }
        Get_char(in, in_ch);
    }
    return true;
}

bool Object::Read(std::string path) {
    std::ifstream in(path);
    char in_ch;
    if (!Get_char(in, in_ch)) {
        return true;
    }
    switch (in_ch) {
    case '"':
        return ReadString(in);
        break;
    case '[':
        return ReadVector(in);
        break;
    case '{':
        return ReadDict(in);
        break;
    default:
        std::string int_str;
        while ('0' <= in_ch && in_ch <= '9') {
            int_str += in_ch;
            in_ch = in.peek();
            if ('0' > in_ch || in_ch > '9') {
                break;
            }
            Get_char(in, in_ch);
        }
        type = Int;
        int_val = std::stoi(int_str);
        return true;
        break;
    }
    return false;
}

void Object::PrintInt(std::ostream& out, std::string given_tab) {
    out << int_val;
}
void Object::PrintString(std::ostream& out, std::string given_tab) {
    out << '"' << str_val << '"';
}
void Object::PrintVector(std::ostream& out, std::string given_tab) {
    std::string tab = given_tab + "  ";
    out << "[\n";
    for (auto i = 0; i < vector_val.size(); ++i) {
        out << tab;
        vector_val[i].Print(out, tab);
        if (i + 1 < vector_val.size()) {
            out << ',';
        }
        out << '\n';
    }
    out << given_tab << "]";
}
void Object::PrintDict(std::ostream& out, std::string given_tab) {
    std::string tab = given_tab + "  ";
    out << "{\n";
    for (auto it = dict_val.begin(); it != dict_val.end(); ++it) {
        if (it != dict_val.begin()) {
            out << ",\n";
        }
        out << tab;
        out << '"' << it->first << '"' <<  ": ";
        it->second.Print(out, tab);
    }
    out << '\n' << given_tab  << "}";
}

void Object::Print(std::ostream& out, std::string tab) {
    switch (type) {
    case Int:
        PrintInt(out, tab);
        break;
    case String:
        PrintString(out, tab);
        break;
    case Vector:
        PrintVector(out, tab);
        break;
    case Dict:
        PrintDict(out, tab);
        break;
    case Null:
        if (tab == "") {
            throw std::exception("Cant print NULL");
        }
        out << "Null";
        break;
    }
}
void Object::Print(std::string path) {
    std::ofstream out(path);
    Print(out, "");
}

Object& Object::Get(std::string name) {
    if (type != Dict) {
        throw std::exception("Can use Object::Get with not Dict Object");
    }
    if (dict_val.find(name) == dict_val.end()) {
        throw std::exception(("There is no \"" + name + "\"in dict").c_str());
    }
    return dict_val[name];
}

Object& Object::Get(int index) {
    if (type != Vector) {
        std::cout << "Can use Object::Get with not Dict Object";
        throw std::exception();
    }
    if (index >= vector_val.size()) {
        std::cout << "There is no \"" + std::to_string(index) + "\" in dict";
        throw std::exception();
    }
    return vector_val[index];
}